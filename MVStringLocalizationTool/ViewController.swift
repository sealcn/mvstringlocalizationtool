//
//  ViewController.swift
//  MVStringLocalizationTool
//
//  Created by Renton on 2018/10/23.
//  Copyright © 2018 Renton. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    @IBOutlet var pathTextView: NSTextField! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    private func checkDuplicated(localizedStrings: [LocalizedString]) {
        for item in localizedStrings {
            let filteredItems = localizedStrings.filter{$0.key == item.key}
            if filteredItems.count > 1 {
                let _ = filteredItems.map{ item -> LocalizedString in
                    print("\(item.module) \(item.key)")
                    return item
                }
                print("\n")
            }
        }
    }
    
    private var selectedPathUrl: URL? = nil
    @IBAction private func chooseFolder(id: Any) {
        guard let window = view.window else { return }
        let panel = NSOpenPanel()
        panel.allowsMultipleSelection = false
        panel.canChooseFiles = false
        panel.canChooseDirectories = true
        panel.beginSheetModal(for: window) { [weak self] (result) in
            if result == NSApplication.ModalResponse.OK {
                let url = panel.urls[0]
                print(url)
                self?.pathTextView.stringValue = url.absoluteString
                self?.selectedPathUrl = url
            }
        }
    }
    
    @IBAction private func startParse(id: Any) {
        
        guard let url = Bundle.main.url(forResource: "peace_input", withExtension: "tsv"),
            let inputString = try? String(contentsOf: url) else {
                assert(false, "Please input 'peace_input' tsv from google doc, and put it in input file")
                return
        }
        let lines = inputString.components(separatedBy: "\n")
        guard lines.count > 1 else {
            assert(false, "Input file is empty")
            return
        }
        
        parseAction(input: lines, languages: ["en", "zh-Hant", "zh-Hans"])
        
    }
    
    @IBAction func parseBitColor(_ sender: Any) {
        
        guard let url = Bundle.main.url(forResource: "bitcolor_input", withExtension: "tsv"),
            let inputString = try? String(contentsOf: url) else {
                assert(false, "Please input 'peace_input' tsv from google doc, and put it in input file")
                return
        }
        let lines = inputString.components(separatedBy: "\n")
        guard lines.count > 1 else {
            assert(false, "Input file is empty")
            return
        }
        
        parseAction(input: lines, languages: ["en", "zh-Hans"])
    }
    
    func parseAction(input: [String], languages: [String]) {
        
        
        guard let selectedPathUrl = selectedPathUrl else {
            pathTextView.stringValue = "Please select path of your main target"
            return
        }
        
        var localizedStrings: [LocalizedString] = []
        let keys = KeyParser.parse(firstLine: input[0])
        for index in 1..<input.count {
            let localizedString = LocalizedString()
            guard localizedString.fillFromLine(line: input[index], keys: keys) else {
                continue
            }
            localizedStrings.append(localizedString)
        }
        checkDuplicated(localizedStrings: localizedStrings)
        
        for language in languages {
            StringOutputer.output(localizedStrings: localizedStrings, pathUrl: selectedPathUrl, lanuagedId: language)//en
        }
        
        //output all en strings
        let enStrings = localizedStrings.compactMap{$0.stringMap["en"]}
    }
    
}


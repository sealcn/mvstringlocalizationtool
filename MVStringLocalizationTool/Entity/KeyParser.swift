//
//  KeyParser.swift
//  MVStringLocalizationTool
//
//  Created by Renton on 2018/10/23.
//  Copyright © 2018 Renton. All rights reserved.
//

import Foundation

class KeyParser {
    static func parse(firstLine: String, seperatingCharacter: String = "\t") -> [String] {
        let parts = firstLine.components(separatedBy: seperatingCharacter)
//        assert(parts.count == 7)
        return parts
    }
}

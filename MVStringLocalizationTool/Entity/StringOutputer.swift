//
//  StringOutputer.swift
//  MVStringLocalizationTool
//
//  Created by Renton on 2018/10/23.
//  Copyright © 2018 Renton. All rights reserved.
//

import Foundation

class StringOutputer {
    static func output(localizedStrings: [LocalizedString], pathUrl: URL, lanuagedId: String) {
        let systemOutputFulsystemOutputUrl = pathUrl.appendingPathComponent("Resource/\(lanuagedId).lproj", isDirectory: true).appendingPathComponent("InfoPlist.strings", isDirectory: false)
        let commonOutputUrl = pathUrl.appendingPathComponent("Resource/\(lanuagedId).lproj", isDirectory: true).appendingPathComponent("Localizable.strings", isDirectory: false)
        
        let systemOutput = localizedStrings.compactMap { (stringItem) -> String? in
            guard stringItem.isSystemKey else { return nil }
            return stringItem.export(lanuageId: lanuagedId)
        }.joined(separator: "\n\n")
        let commonOutput = localizedStrings.compactMap { (stringItem) -> String? in
            guard !stringItem.isSystemKey else { return nil }
            return stringItem.export(lanuageId: lanuagedId)
            }.joined(separator: "\n\n")
        do {
            try systemOutput.write(to: systemOutputFulsystemOutputUrl, atomically: true, encoding: String.Encoding.utf8)
            print("File has been written to \(systemOutputFulsystemOutputUrl)")
            try commonOutput.write(to: commonOutputUrl, atomically: true, encoding: String.Encoding.utf8)
            print("File has been written to \(commonOutputUrl)")
        } catch {
            assert(false, "Fail to write file")
        }
    }
}

//
//  LocalizedString.swift
//  MVStringLocalizationTool
//
//  Created by Renton on 2018/10/23.
//  Copyright © 2018 Renton. All rights reserved.
//

import Foundation

class LocalizedString {
    var module: String = ""
    var key: String = ""
    var comment: String = ""
    var stringMap: [String : String] = [:]
    var isSystemKey: Bool = false //如果是系统的key，导出的时候不加引号，而且需要单独设置key,比如CFBundleDisplayName
    
    public func fillFromLine(line: String, keys: [String], separateCharacter: String = "\t") -> Bool {
//        guard keys.count == 7 else {
//            return false
//        }
        let parts = line.components(separatedBy: separateCharacter)
        
//        guard parts.count == 7 else {
//            return false
//        }
        module = parts[0]
        key = parts[1]
        comment = parts[3]
        isSystemKey = (parts[2] as NSString).boolValue
        for index in 4..<keys.count {
            stringMap[keys[index]] = parts[index]
        }
        return true
    }
    
    public func export(lanuageId: String) -> String? {
        guard let value = stringMap[lanuageId] else {
            return nil
        }
        
        let group: [String] = ["/*\(comment)*/",
            isSystemKey ? "\(key) = \"\(value)\";" : "\"\(key)\" = \"\(value)\";",
        ]
        
        return group.joined(separator: "\n")
    }
}

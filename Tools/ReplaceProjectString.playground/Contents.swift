import Foundation
import PlaygroundSupport

func replace() {
    let stringsUrl = playgroundSharedDataDirectory.appendingPathComponent("allEnStrings.txt")
    guard let inputStrings = try? String(contentsOf: stringsUrl, encoding: String.Encoding.utf8) else {
        print("fail to load file")
        return
    }
    let keys = inputStrings.components(separatedBy: "\n").compactMap{ key -> String? in
        return key.isEmpty ? nil : key
    }
    print(keys)
    
    let projectUrl = playgroundSharedDataDirectory.appendingPathComponent("peace/PeaceMeditation", isDirectory: true)
    let fileManager = FileManager.default
    let enumerator = fileManager.enumerator(at: projectUrl, includingPropertiesForKeys: nil)
    
    var processUrls: [URL] = []
    for url in enumerator! {
        guard let fileUrl = url as? NSURL,
        let fullPath = fileUrl.absoluteString else {
            continue
        }
        if fullPath.hasSuffix(".swift") {
            processUrls.append(fileUrl as URL)
        }
    }
    
    for url in processUrls {
        print("processing \(url)")
        replaceFile(url: url, replaceStrings: keys)
        print("finish processing \(url)\n")
    }
}

func replaceFile(url: URL, replaceStrings: [String]) {
    guard let fileString = try? String(contentsOf: url, encoding: String.Encoding.utf8) else {
        print("fail to read swift file: \(url)")
        return
    }
    var outputString = fileString
    for replaceString in replaceStrings {
        let quotedString = "\"\(replaceString)\""
        let targetString = "\(quotedString).localized()"
        outputString = outputString.replacingOccurrences(of: quotedString, with: targetString)
    }
    do {
        try outputString.write(to: url, atomically: true, encoding: String.Encoding.utf8)
    } catch {
        print("fail to write file \(url)")
    }
}

replace()

//
//  MVStringLocalizationToolTests.swift
//  MVStringLocalizationToolTests
//
//  Created by Renton on 2018/10/23.
//  Copyright © 2018 Renton. All rights reserved.
//

import XCTest
@testable import MVStringLocalizationTool

class MVStringLocalizationToolTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    func testParseFirstLine() {
        guard let url = Bundle.main.url(forResource: "input", withExtension: ""),
            let inputString = try? String(contentsOf: url) else {
                XCTAssert(false)
                return
        }
        let lines = inputString.components(separatedBy: "\n")
        guard lines.count > 1 else {
            XCTAssert(false)
            return
        }
        
        let keys = KeyParser.parse(firstLine: lines.first!)
        XCTAssert(keys[0] == "模块")
        XCTAssert(keys[1] == "Key")
        XCTAssert(keys[2] == "Comment")
        XCTAssert(keys[3] == "en")
        XCTAssert(keys[4] == "zh-Hans")
        XCTAssert(keys[5] == "zh-Hant")
    }
    
    func testPraseLines() {
        guard let url = Bundle.main.url(forResource: "sample", withExtension: ""),
            let inputString = try? String(contentsOf: url) else {
                XCTAssert(false)
                return
        }
        let lines = inputString.components(separatedBy: "\n")
        guard lines.count > 1 else {
            XCTAssert(false)
            return
        }
        
        let keys = KeyParser.parse(firstLine: lines.first!)
        let localizedString = LocalizedString()
        let parsed = localizedString.fillFromLine(line: lines[1], keys: keys)
        XCTAssert(parsed)
        XCTAssert(localizedString.key == "Are u spreading anxiety?")
        XCTAssert(localizedString.comment == "测试样例")
        XCTAssert(localizedString.stringMap["en"] == "Are u spreading anxiety?")
        XCTAssert(localizedString.stringMap["zh-Hans"] == "你在传播焦虑吗？")
        XCTAssert(localizedString.stringMap["zh-Hant"] == "你在傳播焦慮嗎？")
        
    }
}
